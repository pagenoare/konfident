#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os.path
sys.path.insert(
    0, os.path.dirname(
        os.path.realpath(__file__)
    )
)
sys.path.insert(
    0, os.path.dirname(
        os.path.join(
            os.path.realpath(__file__), 'libs'
        )
    )
)

import ircbot
import configuration as c



if __name__ == "__main__":
    try:
        bot = ircbot.IRCBot(server=c.IRC_HOST, port=c.IRC_PORT,
                channel=c.IRC_CHANNEL, password=c.IRC_PASSWORD,
                username=c.IRC_REALNAME, nickname=c.IRC_NAME)
        # start the loop
        bot.run()
    except KeyboardInterrupt:
        bot.stop()
        print "Exit."
