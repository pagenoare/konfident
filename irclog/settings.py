import os.path
import sys

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('Piotr Husiatynski', 'please@dontspam.me'),
)

PROJECT_PATH = sys.path[0]
MANAGERS = ADMINS

DATABASE_ENGINE = 'sqlite3'
#DATABASE_NAME = os.path.join(PROJECT_PATH, 'irclog', 'django_sqlite3_database.db')
DATABASE_NAME = '/home/irc/public_html/django_sqlite3_database.db'
DATABASE_USER = ''
DATABASE_PASSWORD = ''
DATABASE_HOST = ''
DATABASE_PORT = ''

TIME_ZONE = 'Europe/Warsaw'
LANGUAGE_CODE = 'pl'
SITE_ID = 1
USE_I18N = True
MEDIA_ROOT = ''
MEDIA_URL = '/irc/media/'
ADMIN_MEDIA_PREFIX = '/media/'
SECRET_KEY = ''

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.doc.XViewMiddleware',
)

ROOT_URLCONF = 'public_html.urls'

TEMPLATE_DIRS = (
    "/home/irc/public_html/templates",
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'public_html.log',
    'public_html.error_handlers',
)
