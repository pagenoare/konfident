from django.db import models


class User(models.Model):
    nick_name = models.CharField(max_length=20)

    def __unicode__(self):
        return unicode(self.nick_name)

    class Admin():
        pass

class Message(models.Model):
    user_id = models.ForeignKey(User)
    date = models.DateTimeField(auto_now_add=True)
    text = models.TextField()

    class Meta:
        ordering = ["-date"]

    def __unicode__(self):
        return unicode(self.text)

    class Admin():
        pass


