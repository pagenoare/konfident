#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.shortcuts import render_to_response
from public_html.log.models import Message, User
from django.db.models import Q
import time

def index(request, from_msg=0, template='log/index.html'):
    from_msg = int(from_msg)
    to_msg = from_msg + 35
    prev_msg = from_msg - 35
    t = time.time()
    msg_list = list(Message.objects.select_related('user__nick_name')[from_msg:to_msg])
    t = time.time() - t
    msg_list.reverse()
    return render_to_response(template, locals())

def user(request, username, template='log/user.html'):
    try:
        user = User.objects.get(nick_name=username)
        user_msg = user.message_set.all()[:20]
        error_message = None
    except User.DoesNotExist:
        user = None
        user_msg = None
        error_message = u'Nie znaleziono w bazie użytkownika: %s' % username
    return render_to_response(template, {
            'user' : user,
            'user_msg' : user_msg,
            'error_message' : error_message,
        })

def search(request, template='log/search.html'):
    query = request.GET.get('q', '')
    if query:
        qset = (Q(text__icontains=query))
        results = Message.objects.filter(qset)[:20]
    else:
        results = []
    return render_to_response(template, {
                "results": results,
                "query": query,
            })
