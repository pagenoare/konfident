
from django.conf.urls.defaults import *

urlpatterns = patterns('public_html.log.views',
    (r'^$',                             'index'),
    (r'^old/(\d+)/$',               'index'),
    (r'^user/(?P<username>[^/]+)/$',     'user'),
    (r'^search/$',                      'search'),
)
