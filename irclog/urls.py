from django.conf.urls.defaults import *

urlpatterns = patterns('',
    # admin
    (r'^irc/admin/', include('django.contrib.admin.urls')),

    (r'^irc/', include('public_html.log.urls')),
)


handler404 = 'public_html.error_handlers.views.h_404'
handler500 = 'public_html.error_handlers.views.h_500'
