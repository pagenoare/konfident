# -*- coding: utf-8-*-

from django.http import HttpResponse
from django.shortcuts import render_to_response


class Error(object):
    def __init__(self, type, text=''):
        self.type = type
        self.text = unicode(text)


def h_404(request):
    return render_to_response('error_handlers/base.html', {
        'error' : Error(404, u'Nie znaleziono strony.'),
    })


def h_500(request):
    return render_to_response('error_handlers/base.html', {
        'error' : Error(500, u'Wystąpił błąd serwera.'),
    })

