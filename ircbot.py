import os
import sys

from utils.plugins import plugins_load
import configuration as conf
# libs
from libs import irclib

global conf

class IRCBot(irclib.SimpleIRCClient):
    def __init__(self, **kwds):
        self.conf = kwds
        irclib.SimpleIRCClient.__init__(self)
        # load plugins
        plug_path = os.path.join(os.path.dirname(__file__), conf.PLUGIN_PATH)
        plug_events = plugins_load(plug_path)
        for event in plug_events:
            for priority, handler in plug_events[event]:
                self.ircobj.add_global_handler(event, handler, priority)

    def stop(self):
        # todo - stop threads
        pass

    def run(self):
        # join to channel
        self.ircobj.add_global_handler('welcome', self.channel_join)
        self.connect(
            server=self.conf['server'],
            port=self.conf['port'],
            password=self.conf['password'],
            username=self.conf['username'],
            nickname=self.conf['username']
        )
        self.start()

    def channel_join(self, connection, event):
        connection.join(self.conf['channel'])
