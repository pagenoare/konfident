import sys
import os


def plugins_load(plugins_path, prefix="on_", plugsuffix='.py'):
    handlers = {}
    # add plugins_path to sys.path, that I could import from there
    if plugins_path not in sys.path:
        sys.path.insert(0, plugins_path)
    # import all plugins from `plugins_path`
    for modfile in os.listdir(plugins_path):
        if modfile.startswith('__'):
            continue
        if modfile.endswith(plugsuffix):
            modname = modfile.split('.')[0]
            mod = __import__(modname)
            for event in mod.handlers:
                for h in mod.handlers[event]:
                    if not event in handlers:
                        handlers[event] = []
                    handlers[event].append(h)
    return handlers

if __name__ == "__main__":
    print plugins_load('plugins')
