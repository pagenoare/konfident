#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib2 as u

COMMAND = ':google'

class Google(object):

    def __call__(self, connection, event):
        eventtype = event.eventtype()
        nick, host = event.source().split('!', 1)
        if eventtype == 'pubmsg':
            msg = event.arguments()[0].split()
            if len(msg) > 1:
                if msg[0] == COMMAND:
                    self.search(nick, msg[1])

    def search(self, nick, what):
        what = u.quote(what, '')
        req = u.Request('http://www.google.pl/search?hl=pl&q=' + what)
        req.add_header('User-agent', 'Mozilla/5.0')
        r = u.urlopen(req)
        print r.read()



handlers = {
    'pubmsg': [(0, Google()), ],
}
