import time
import threading
import os
import pickle

from libs import feedparser
import configuration as conf

FEEDS = (
    #(0, '@RSS Vortal: ', 'http://pl.python.org/feed/atom.atom'),
    (0, '@RSS Forum: ', 'http://pl.python.org/forum/index.php?type=rss;'
        'action=.xml'),
    (0, '@RSS Planeta: ', 'http://pl.python.org/planeta/feeds/atom'),
    (1, '@RSS Kopie robocze: ', 'http://pl.python.org/feed/5Es1kRmVObmtf'
        'fWVkeHmGgMRV9w3d3Ng3EkGsl0nfxCnmoyycj.atom'),
)

FEED_CHECK_TIME = 60 * 10
PICKLE_FILE = 'feed_plugin_data.pickle'
PICKLE_FILE_USERS = 'users.pickle'


class FeedReader(threading.Thread):
    def __init__(self):
        super(FeedReader, self).__init__(name='FeedReader_Thread')
        self._stopevent = threading.Event()
        self.connection = None
        self.users = None
        self.last_feed_check = 0
        self.feed_pile = {
            0: [], 1 : []
        }
        self.read_pickle()
        # run the thread
        self.start()

    def __del__(self):
        self.join()

    def __call__(self, connection, event):
        self.connection = connection

    def read_pickle(self):
        '''read last_feed data'''
        self.last_feed = {}
        for f in FEEDS:
            self.last_feed[f[2]] = (0, 0, 0, 0, 0, 0, 0, 0, 0)

        if os.path.isfile(PICKLE_FILE):
            cfile = open(PICKLE_FILE, 'r')
            self.last_feed.update(pickle.loads(cfile.read()))
            cfile.close()

    def write_pickle(self):
        '''serialize last_feed data'''
        if os.path.isfile(PICKLE_FILE):
            os.remove(PICKLE_FILE)
        cfile = open(PICKLE_FILE, 'w')
        pickle.dump(self.last_feed, cfile)
        cfile.close()

    def read_feed(self):
        for flvl, ftext, furl in FEEDS:
            f = feedparser.parse(furl)
            feed_titles = []
            for e in f['entries'][::-1]:
                # check the date
                if self.is_new(furl, e.updated_parsed) and \
                        not e.title in map(lambda x: x[0], feed_titles):
                    feed_titles.append((e.title, e.link))
            while feed_titles:
                # part titles list
                msg = []
                for feed, url in feed_titles[:5]:
                    msg.append('\x02'+ftext+'\x02' + feed + ' :: %s' % url)
                feed_titles = feed_titles[5:]
                self.feed_pile[flvl].extend(msg)

    def is_new(self, feed, date):
        for pos, d in enumerate(date):
            if d < self.last_feed[feed][pos]:
                return False
            elif d > self.last_feed[feed][pos]:
                self.last_feed[feed] = date
                return True

    def join(self):
        self._stopevent.set()
        threading.Thread.join(self, None)

    def run(self):
        while not self._stopevent.isSet():
            # thread sleep
            time.sleep(3)
            # check the feeds
            if self.last_feed_check < time.time() - FEED_CHECK_TIME:
                self.read_feed()

            if self.users is None and os.path.isfile(PICKLE_FILE_USERS):
                picklefile = open(PICKLE_FILE_USERS, 'r')
                users = pickle.loads(picklefile.read())
                self.users = []
                for user in users:
                    if 'r' in users.get(user)[0].split(','):
                        self.users.append(user)
                picklefile.close()

            # if new messages, shot them
            if self.connection is not None:
                if self.feed_pile.get(0):
                    self.last_feed_check = time.time()
                    for msg in self.feed_pile.get(0):
                        msg = msg.replace('&lt;', '<').replace(
                            '&gt;', '>').replace('&quot;', "'").replace(
                            '&amp;', '&')
                        self.connection.privmsg(
                            conf.IRC_CHANNEL, msg.encode('utf-8'))
                        time.sleep(1)
                if self.feed_pile.get(1):
                    self.last_feed_check = time.time()
                    for nickname in self.users:
                        for msg in self.feed_pile.get(1):
                            self.connection.privmsg(nickname,
                                msg.encode('utf-8'))
                # sleep between messages - IRC flood protection
                self.feed_pile = {
                    0: [], 1 : []
                }
                # save new data messages
                self.write_pickle()


handlers = {
    'join': [(0, FeedReader()), ],
}

if __name__ == "__main__":
    f = FeedReader()
    for x in range(10):
        print '>> feed : ', f('')
        time.sleep(2)
    f.threadRun = False
    del f
