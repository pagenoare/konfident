import sqlalchemy as sql
import sqlalchemy.exc
from time import strftime, sleep
import datetime

# for more info see
# http://www.sqlalchemy.org/docs/04/intro.html#overview_sqlalchemy
DATABASE_PATH = "sqlite:///irclog/django_sqlite3_database.db"

class DatabaseConnection(object):
    """Database connection"""
    def __init__(self, dblink):
        self.connect(dblink)

    def connect(self, dblink):
        """Create database connection"""
        self.database = sql.create_engine(dblink)
        metadata =  sql.MetaData(self.database)
        self.message = sql.Table('log_message', metadata, autoload=True)
        self.user = sql.Table('log_user', metadata, autoload=True)

    def add_msg(self, user_id, text):
        """Add new message. Doesn't check if user_id exist"""
        # example 2008-02-17 18:03:46.072746
        # TODO
        #now = strftime("%Y-%m-%d %H:%M:%S.000000")
        now = datetime.datetime.now()
        try:
            text = unicode(text, encoding="utf-8")
        except:
            text = text.decode('ascii', 'replace')
        i = self.message.insert()
        added = False
        while not added:
            try:
                i.execute(user_id_id=user_id, date=now, text=text)
                added = True
            except sqlalchemy.exc.OperationalError:
                sleep(0.05)

    def add_user(self, nick):
        """Add new user. Doesn't check if allready exist
        Return his new id.
        """
        i = self.user.insert()
        added = False
        while not added:
            try:
                i.execute(nick_name=nick, first_name='', last_name='',
                    about='', homepage='', email='', avatar='')
                added = True
            except sqlalchemy.exc.OperationalError:
                sleep(0.05)
        return self.get_user(nick)

    def get_user(self, nick):
        """Find user and returns his id, or None if doesn't exist"""
        s = self.user.select(self.user.c.nick_name == nick)
        try:
            return s.execute().fetchone()[0]
        except TypeError:
            return None

class Log(object):
    def __init__(self):
        self.db = DatabaseConnection(DATABASE_PATH)
        self.users = {}

    def __call__(self, connection, event):
        eventtype = event.eventtype()
        if eventtype in ('join', 'quit', 'part'):
            #msg = '--    %s' % eventtype
       	    return
        elif eventtype == 'pubmsg':
            msg = event.arguments()[0]
        elif eventtype == 'action':
            #msg = '--    %s' % event.arguments()[0]
            return
        else:
            # do nothing, just end
            return
        nick, host = event.source().split('!', 1)
        if not nick in self.users.keys():
            self.users[nick] = self.db.get_user(nick)
            if not self.users[nick]:
                self.users[nick] = self.db.add_user(nick)
        # add message to database
        self.db.add_msg(self.users[nick], msg)



handlers = {
    'all_events': [(0, Log()), ],
}
