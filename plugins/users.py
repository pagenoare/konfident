#!/usr/bin/env python
# -*- coding: utf-8 -*-

import pickle
import os

PICKLE_FILE = 'users.pickle'
PERMISSIONS = {':op': 'o', ':voice': 'v', ':admin': 'a', 'r': None}
LIST_CMD = ':users'
LIST_FORMAT = '%s | Flagi: %s'


class Users(object):
    def __init__(self):
        self.users = {}
        self.get_users()

    def __call__(self, connection, event):
        eventtype = event.eventtype()
        if eventtype in ['pubmsg', 'privmsg']:
            nick, host = event.source().split('!')
            msg = event.arguments()[0].split()
            if self.users.has_key(nick):
                if self.users[nick][1] == host:
                    perm = self.users[nick][0].split(',')
                    try:
                        if PERMISSIONS[msg[0]] in perm:
                            eval('self.%s(connection, event)' % msg[0][1:])
                    except KeyError:
                        pass
            if len(msg) > 0:
                if msg[0] == LIST_CMD:
                    for i in self.users:
                        connection.notice(
                            event.source().split('!')[0],
                            LIST_FORMAT % (i, self.users[i][0])
                        )

    def get_users(self):
        if os.path.isfile(PICKLE_FILE):
            picklefile = open(PICKLE_FILE, 'r')
            self.users = pickle.loads(picklefile.read())
            picklefile.close()

    def save_changes(self):
        if os.path.isfile(PICKLE_FILE):
            os.remove(PICKLE_FILE)
        pickle_file = open(PICKLE_FILE, 'w')
        pickle.dump(self.users, pickle_file)
        pickle_file.close()

    def op(self, connection, event):
        nick = event.source().split('!')[0]
        connection.send_raw('MODE %s +o %s' % (event.target(), nick))

    def voice(self, connection, event):
        nick = event.source().split('!')[0]
        connection.send_raw('MODE %s +v %s' % (event.target(), nick))

    def admin(self, connection, event):
        msg = event.arguments()[0].split()
        if len(msg) > 4 and msg[1] == 'add':
            self.users[msg[2]] = [msg[3], msg[4]]
        elif len(msg) > 1 and msg[1] == 'delete':
            del self.users[msg[2]]
        elif len(msg) > 1 and msg[1] == 'save':
            self.save_changes()


handlers = {
    'pubmsg': [(0, Users()), ],
    'privmsg': [(0, Users()), ],
}
