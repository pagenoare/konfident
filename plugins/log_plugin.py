from time import strftime
import os

class Log(object):
    def __init__(self):
        self.logFolder = '/home/irc/irclogs'

        if not os.path.exists(self.logFolder):
            os.makedirs(self.logFolder)


    def __call__(self, connection, event):
        self.logfile = file(os.path.join(self.logFolder, strftime("%Y.%m.%d.log")), 'a')
        now = strftime("%H:%M:%S")
        eventtype = event.eventtype()
        if eventtype in ('join', 'quit', 'part'):
            nick, host = event.source().split('!', 1)
            self.logfile.write('%s -!- %s [%s] has %s #python.pl []\n' % (now, nick, host, {'join':'joined'}.get(eventtype, eventtype)))
        if eventtype == 'pubmsg':
            msg = event.arguments()[0]
        elif eventtype == 'action':
            msg = '--    %s' % event.arguments()[0]
        else:
	    # print eventtype
            # do nothing, just end
            return
        nick, host = event.source().split('!', 1)
        self.logfile.write('%s <%s> %s\n' % (now, nick, msg))
        self.logfile.close()



handlers = {
    'all_events' : [(0, Log()), ],
}
