import time

from libs import irclib

import configuration as conf


def connect(connection, event):
    while True:
        try:
            time.sleep(5)
            reconnect(connection)
        except irclib.ServerConnectionError as e:
            print(e)
        else:
            break

def reconnect(connection):
    connection.connect(
        server=conf.IRC_HOST, port=conf.IRC_PORT,
        password=conf.IRC_PASSWORD, username=conf.IRC_REALNAME,
        nickname=conf.IRC_NAME
    )

handlers = {
    'disconnect': [(0, connect), ],
}
