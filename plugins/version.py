#!/usr/bin/env python
# -*- coding: utf-8 -*-

COMMAND = [':version', ':v', ':about']
MSG = "%s: Jestem \x02Konfident\x02, wersja \x02%s\x02. " \
      "Zrodla: <http://bitbucket.org/pagenoare/konfident/src/>. "
class Version(object):

    def __call__(self, connection, event):
        eventtype = event.eventtype()
        nick, host = event.source().split('!', 1)
        if eventtype == 'pubmsg':
            msg = event.arguments()[0].split()
            if len(msg) > 0:
                if msg[0] in COMMAND:
                    f = open('VERSION', 'r')
                    connection.privmsg(event.target(), MSG % (nick, f.readlines()[0]))
                    f.close()


handlers = {
    'pubmsg' : [(0, Version()),],
}
