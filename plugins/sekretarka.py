#!/usr/bin/env python
# -*- coding: utf-8 -*-


import os
import time
import pickle

COMMAND = ':msg'
MESSAGE_FORMAT = '%(for_nick)s: \x02[%(date)s]\x02 %(from_nick)s: %(text)s '
PICKLE_FILE = 'sekretarka_plugin_data.pickle'


class Sekretarka(object):
    def __init__(self):
        self.messages = {}
        self.read_pickle()

    def __call__(self, connection, event):
        eventtype = event.eventtype()
        if eventtype in ['pubmsg', 'privmsg']:
            # save message
            msg = event.arguments()[0].split()
            if len(msg) > 2:
                if msg[0] == COMMAND:
                    self.save_msg(connection, event)
            # send message
        if eventtype in ['join', 'pubmsg']:
            # send message
            self.return_msg(connection, event)

    def read_pickle(self):
        if os.path.isfile(PICKLE_FILE):
            picklefile = open(PICKLE_FILE, 'r')
            self.messages = pickle.loads(picklefile.read())
            picklefile.close()

    def write_pickle(self):
        if os.path.isfile(PICKLE_FILE):
            os.remove(PICKLE_FILE)
        pickle_file = open(PICKLE_FILE, 'w')
        pickle.dump(self.messages, pickle_file)
        pickle_file.close()

    def return_msg(self, connection, event):
        nick, host = event.source().split('!', 1)
        if nick in self.messages:
            for msg in self.messages[nick]:
                # send
                connection.privmsg(event.target(), MESSAGE_FORMAT % msg)
                time.sleep(2)
            # remove messages from dict
            del self.messages[nick]
            self.write_pickle()

    def save_msg(self, connection, event):
        msg = event.arguments()[0].split()
        nick = msg[1]
        msgfrom = event.source().split('!', 1)[0]
        if not nick in self.messages:
            self.messages[nick] = []
        # message structure
        info_data = {
                "for_nick" : nick,
                "from_nick" : msgfrom,
                "date" : time.strftime("%d.%m.%Y %H:%M"),
                "text" : ' '.join(msg[2:]),
                }
        self.messages[nick].append(info_data)
        self.write_pickle()

# plugin class instance
sekretarka = Sekretarka()

handlers = {
    'join': [(0, sekretarka), ],
    'pubmsg': [(0, sekretarka), ],
    'privmsg': [(0, sekretarka), ],
}


if __name__ == "__main__":
    sekretarka.write_pickle()
